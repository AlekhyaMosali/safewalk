package com.example.safewalk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;


public class LoginActivity extends Activity {
	static final String TAG = "Safe Walk";

	private Button loginButton;
	private Dialog progressDialog;
	private ParseUser currentUser;
	private ArrayList<ParseObject> emergencyContactList;
	private ArrayList<ParseUser> notifiableEmergencyContacts;
	private SafeWalkApplication application;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application = (SafeWalkApplication) getApplication();
		
		setContentView(R.layout.activity_login);
		
	    // Add code to print out the key hash
	    try {
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.example.safewalk", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }

		loginButton = (Button) findViewById(R.id.loginButton);
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onLoginButtonClicked();
			}
		});

		// Check if there is a currently logged in user
		// and it's linked to a Facebook account.
		 currentUser = ParseUser.getCurrentUser();
		if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
			// Go to the user info activity
			application.currentUser = currentUser;
			showUserDetailsActivity();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.global, menu);
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);

		
	}

	private void onLoginButtonClicked() {
		LoginActivity.this.progressDialog = 
				ProgressDialog.show(LoginActivity.this, "", "Logging in...", true);
		
		List<String> permissions = Arrays.asList("public_profile", "email", "user_friends");
		// NOTE: for extended permissions, like "user_about_me", your app must be reviewed by the Facebook team
		// (https://developers.facebook.com/docs/facebook-login/permissions/)
		
		ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {
				LoginActivity.this.progressDialog.dismiss();
				if (user == null) {
					Log.d(LoginActivity.TAG, "Uh oh. The user cancelled the Facebook login.");
				} else if (user.isNew()) {
					Log.d(LoginActivity.TAG, "User signed up and logged in through Facebook!");
	                // We just store our selection in the Application for other activities to look at.

	                application.currentUser = user;
	                currentUser = user;
					showUserDetailsActivity();
				} else {
					Log.d("demo", "User logged in through Facebook!");
					application.currentUser = user;
					currentUser = user;

					showUserDetailsActivity();
				}
				

			}
		});
	}

	private void showUserDetailsActivity() {
		//check if at least one of the emergency contacts have installed the app on their devices
		ParseQuery<ParseObject> query = ParseQuery.getQuery("EmergencyContact");
		query.whereEqualTo("user", currentUser);
		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				//list of all the emergency contacts of the user
				if(objects.size() >0){
					emergencyContactList = (ArrayList<ParseObject>) objects;
					ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
					userQuery.findInBackground(new FindCallback<ParseUser>() {

						@Override
						public void done(List<ParseUser> objects,
								ParseException e) {
							
							if(objects.size()>0){
								notifiableEmergencyContacts = new ArrayList<ParseUser>();
								for (ParseUser user :objects){
									for (ParseObject emergencyContact:emergencyContactList){
										String userId = user.getString("userID");
										String emergencyUserId = emergencyContact.getString("userID");
										if(userId == null ? emergencyUserId == null : userId.equals(emergencyUserId)){
											notifiableEmergencyContacts.add(user);
										}
									}
								}
								if(notifiableEmergencyContacts.size()>0){
									
									//set the list of emergency parse users

									application.emergencyParseUserContacts = notifiableEmergencyContacts;
									
									Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
									startActivity(intent);
								}else{
									Intent intent = new Intent(getApplicationContext(), ConfigureFriendsActivity.class);
									startActivity(intent);
								}
							}
						}
						
					});
				}else{
					Intent intent = new Intent(getApplicationContext(), ConfigureFriendsActivity.class);
					startActivity(intent);
				}
			}
		});

		//simple prints the name of the user from Graph API
		if (ParseFacebookUtils.getSession().isOpened())
        {
            Request.executeMeRequestAsync(ParseFacebookUtils.getSession(), new Request.GraphUserCallback()
            {
                @Override
                public void onCompleted(GraphUser graphUser, Response response)
 {
							if (graphUser != null) {
								// progressdialog.dismiss();
								Toast toast = Toast.makeText(
										getApplicationContext(),
										"Thanks, "
												+ graphUser.getName()
												+ ". You are now successfully logged in through Facebook!",
										Toast.LENGTH_LONG);
								toast.setGravity(Gravity.TOP, 0, 20);
								toast.show();
								finish();
								application.userName = graphUser.getName();
								application.currentUser.put("userID",
										graphUser.getId());
								application.currentUser.put("userRealName",graphUser.getName());
								application.currentUser
										.saveInBackground(new SaveCallback() {

											@Override
											public void done(ParseException e) {
												// TODO Auto-generated method
												// stub
												Log.d("demo", "saved userID");
											}
										});

								// Associate the device with a user
								ParseInstallation installation = ParseInstallation
										.getCurrentInstallation();
								installation.put("user", currentUser);
								installation.saveInBackground();

								PushService.setDefaultPushCallback(
										getApplicationContext(),
										TakeAWalkActivity.class);
								ParseInstallation.getCurrentInstallation()
										.saveInBackground();

								// fetch the walk points if any
								ParseQuery<ParseObject> query = ParseQuery
										.getQuery("Walk");
								query.whereEqualTo("user", currentUser);
								query.findInBackground(new FindCallback<ParseObject>() {

									@Override
									public void done(List<ParseObject> objects,
											ParseException e) {
										if (objects.size() > 0) {
											application.walkPoints = (ArrayList<ParseGeoPoint>) objects
													.get(0).get("walkPoints");
										} else {
											application.walkPoints = new ArrayList<ParseGeoPoint>();
										}
									}
								});
							}
						}
            });
        }
	}
}
