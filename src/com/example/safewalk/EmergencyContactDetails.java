package com.example.safewalk;

import java.util.List;

import javax.security.auth.callback.Callback;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.widget.ProfilePictureView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class EmergencyContactDetails extends Activity {

	String name;
	String userID;
	ParseUser currentUser;
	boolean hasInstalled;
	TextView nameTv;
	TextView phoneNumberEt;
	ProfilePictureView profilePicImageView;

	static final int REQ_CODE = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency_contact_details);
		this.setTitle("Emergency Friend Details");

		name = getIntent().getStringExtra("name");
		userID = getIntent().getStringExtra("userGraphID");
		hasInstalled = false;

		nameTv = (TextView) findViewById(R.id.contactName);
		nameTv.setText(name);

		phoneNumberEt = (TextView) findViewById(R.id.contactPhone);
		profilePicImageView = (ProfilePictureView) findViewById(R.id.friendProfilePicture);
		profilePicImageView.setProfileId(userID);

		// check whether the emergency contact has started to use the app
		// this can be done by checking for the name of the emergency contact in
		// the "Users" table

		ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
		userQuery.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> objects, ParseException e) {
				if (e == null) {
					for (ParseUser object : objects) {
						try {
							object.fetchIfNeeded();
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						// http://stackoverflow.com/questions/24560906/how-to-get-authdata-info-from-parseuser-object
						// apparently we cannot get the authdata from Parse :(
						Log.d("demo",
								object.toString() + object.getString("userID")
										+ "graph User:" + userID);
						String grahUserId = object.getString("userID");
						// if(grahUserId.equals(userID))
						if (grahUserId != null && grahUserId.equals(userID)) {
							Log.d("demo", "---user has installed----");
							hasInstalled = true;
							/*Toast.makeText(
									getApplicationContext(),
									"This user can be sent an emergency notification right away",
									Toast.LENGTH_SHORT).show();*/
							break;
						}

					}

				} else {
					// Something went wrong.
				}
			}
		});
		// ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("User");
		// userQuery.findInBackground(new FindCallback<ParseObject>() {
		//
		// @Override
		// public void done(List<ParseObject> objects, ParseException e) {
		// // TODO Auto-generated method stub
		//
		// }
		// });

		// Check if the the emergency contact details are already present

		ParseQuery<ParseObject> query = ParseQuery.getQuery("EmergencyContact");
		query.whereEqualTo("name", name);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if (objects.size() > 0) {
					if (objects.get(0).getString("name").equals(name)) {
						Log.d("demo", "---contact already exists----");
						phoneNumberEt.setText(objects.get(0).getString(
								"contact"));
					}
				} else {
					phoneNumberEt.setText("Please update Phone number");
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.emergency_contact_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// on clicking the update phone number image View
	public void onEditContactCliked(View v) {
		Intent intent = new Intent(EmergencyContactDetails.this,
				EditEmergencyContactActivity.class);
		intent.putExtra("contactName", name);
		intent.putExtra("contactID", userID);
		startActivityForResult(intent, REQ_CODE);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQ_CODE) {
			if (resultCode == RESULT_OK
					&& data.getExtras().containsKey("updated_PhoneNo")) {
				phoneNumberEt.setText(data.getExtras().getString(
						"updated_PhoneNo"));
			} else {
				Toast.makeText(this, "Contact Number is not updated",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onDeleteFriendButtonClicked(View v) {
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseQuery<ParseObject> deleteQuery = ParseQuery.getQuery("EmergencyContact");
		deleteQuery.whereEqualTo("user", currentUser);
		deleteQuery.getFirstInBackground(new GetCallback<ParseObject>() {
			
			@Override
			public void done(ParseObject object, ParseException e) {
				if(e == null){
					object.deleteInBackground();
					Log.d("demo","Delete Friend Successful");
					finish();
				}else{
					Log.d("demo", "Delete Friend failed");
					finish();
				}
			}
		});
	}

	public void onChangeFriendButtonClicked(View v) {

	}

}
