package com.example.safewalk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class EditEmergencyContactActivity extends Activity {

	String contactName;
	String contactID;
	EditText etContactPhone;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_emergency_contact);
		this.setTitle("Update Contact");
		contactName = getIntent().getStringExtra("contactName");
		contactID = getIntent().getStringExtra("contactID");
		
		etContactPhone = (EditText) findViewById(R.id.etContactPhone);
		
	}
	public void onSaveButtonClicked(View v){
		if(TextUtils.isEmpty(etContactPhone.getText().toString())){
			Toast.makeText(this,
					"Please enter the emergency contact of the user",
					Toast.LENGTH_SHORT).show();
		}
		else{
			ParseObject emergencyContact = new ParseObject("EmergencyContact");
			emergencyContact.put("name", contactName);
			emergencyContact.put("contact", etContactPhone.getText().toString());
			emergencyContact.put("userID", contactID);
            SafeWalkApplication application = (SafeWalkApplication) getApplication();

			emergencyContact.put("user", application.currentUser);
			Log.d("demo","starting to save contact");
			emergencyContact.saveInBackground(new SaveCallback() {
				
				@Override
				public void done(ParseException e) {
					// TODO Auto-generated method stub
					Log.d("demo","contact saved");
					Intent intent = new Intent();
					intent.putExtra("updated_PhoneNo", etContactPhone.getText().toString());
					setResult(RESULT_OK, intent);
					finish();
				}
			});

		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_emergency_contact, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
