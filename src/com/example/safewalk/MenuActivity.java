package com.example.safewalk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.Session;
import com.parse.ParseUser;

public class MenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
	}

	public void onEditFriendsButtonClicked(View v) {

		Intent intent = new Intent(MenuActivity.this,SafetyNetwork.class);
		intent.putExtra("intentAction", SafeWalkApplication.EDIT_FRIENDS_ACTION);
		startActivity(intent);

	}

	public void onTakeAWalkButtonClicked(View v) {
		Intent intent = new Intent(MenuActivity.this, TakeAWalkActivity.class);
		startActivity(intent);
	}

	public void onMonitorFriendsButtonClicked(View v) {
		Intent intent = new Intent(this, MapActivity.class);
		//on Monitoring Friends it is assuemd that the user is safe
		intent.putExtra("walkStatus", "Safe");
		intent.putExtra("locationBroadcast", false);
		startActivity(intent);

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			ParseUser.logOut();
			
			finish();
			//log out from facebook
			Session.getActiveSession().closeAndClearTokenInformation();
			
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
