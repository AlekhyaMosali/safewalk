package com.example.safewalk;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.safewalk.SafeWalkApplication.IEmergencyContactsFetch;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class MapActivity extends Activity implements IEmergencyContactsFetch {
	GoogleMap gMap;
	ProgressDialog progressDialog;
	HashMap<String, Marker> markersList;
	boolean isFirstTimeFetch;
	String[] status = { "Risk", "Little Risk", "Safe" };
	int spinnerItemSelectionMadeCount;
	HashMap<String, String> userSafetyStatusList;
	ArrayList<String> emergencyUserNameList;
	Map<String, List<LatLng>> emergencyContactsGeoPoints;

	ArrayList<ParseUser> emergencyContactList;

	Marker myMarker;

	SafeWalkApplication application;

	String currentSelectedUser;

	Spinner emergencyContactSpinner;
	Spinner safetyStatusSpinner;
	
	Switch locationBroadcastSwitch;
	Button homeSafeButton;

	Timer timer;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		application = (SafeWalkApplication) getApplication();

		isFirstTimeFetch = true;

		userSafetyStatusList = new HashMap<String, String>();
		emergencyUserNameList = new ArrayList<String>();

		emergencyContactsGeoPoints = new HashMap<String, List<LatLng>>();

		safetyStatusSpinner = (Spinner) findViewById(R.id.spinner1);
		emergencyContactSpinner = (Spinner) findViewById(R.id.spinner2);
		
		locationBroadcastSwitch = (Switch)findViewById(R.id.Switch1);
		homeSafeButton = (Button) findViewById(R.id.button2);
		

		showProgress("Syncing Emergency Contacts");

		timer = new Timer();
		timer.schedule(new TimerTask() {

			public void run() {

				fetchWalkInfo();

			}
		}, new Date(), 2000);

		// default is Safe in case of Monitor Friends
		final String safetyStatusFromIntent = getIntent().getExtras()
				.getString("walkStatus", "Safe");
		
		final Boolean shouldBroadcastLocation = getIntent().getExtras()
				.getBoolean("locationBroadcast", true);
		
//		if(shouldBroadcastLocation){
			locationBroadcastSwitch.setChecked(shouldBroadcastLocation);
//		}
		

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				MapActivity.this, android.R.layout.simple_spinner_item, status);

		// Specify the layout to use when the list of choices
		// appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		safetyStatusSpinner.setAdapter(adapter);

		// looks like the first time the spinners default item selected will
		// trigger an OnItemSelected call.
		// we need to get rid of this as any item selected change will send a
		// push notification to the users that a status change
		// was made
		// initially set the status passed by intent
		//

		safetyStatusSpinner
				.setSelection(getIndexForStatus(safetyStatusFromIntent));
		spinnerItemSelectionMadeCount = 0;
		safetyStatusSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, final int position, long id) {

						final String safetyStatus = getStatusForIndex(position);
						application.setCurrentSafetyStatus(safetyStatus);
						spinnerItemSelectionMadeCount += 1;
						if (spinnerItemSelectionMadeCount > 1) {
							ParseQuery<ParseObject> query = ParseQuery
									.getQuery("Walk");
							query.whereEqualTo("user",
									ParseUser.getCurrentUser());
							query.getFirstInBackground(new GetCallback<ParseObject>() {

								@Override
								public void done(ParseObject object,
										ParseException e) {
									object.put("safetyStatus", safetyStatus);
									Log.d("demo", "safety status put: "
											+ safetyStatus);
									object.saveInBackground(new SaveCallback() {

										@Override
										public void done(ParseException e) {
											Log.d("demo", "Safety status saved");
											final SafeWalkApplication application = (SafeWalkApplication) getApplication();
											ArrayList<ParseUser> emergencyContacts = application.emergencyParseUserContacts;
											ParsePush parsePush = new ParsePush();
											ParseQuery pQuery = ParseInstallation
													.getQuery(); // <--
																	// Installation
																	// query
											pQuery.whereContainedIn("user",
													emergencyContacts); // <--
																		// you'll
																		// probably
																		// want
																		// to
																		// target
																		// someone
																		// that's
																		// not
																		// the
																		// current
																		// user,
																		// so
																		// modify
																		// accordingly
											parsePush
													.sendMessageInBackground(
															application.userName
																	+ " has changed safety status to "
																	+ getStatusForIndex(position),
															pQuery);

										}
									});

								}
							});
						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}

				});

		setUpMapIfNeeded();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (gMap == null) {
			gMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (gMap != null) {

			}
		}
	}

	private void showProgress(String message) {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(this);
		}
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show();
	}

	private void hideProgress() {
		progressDialog.dismiss();
	}

	@Override
	public void fetchDone(final ArrayList<ParseUser> emergencyContactsList) {

		emergencyContactList = emergencyContactsList;
		hideProgress();
		emergencyContactsGeoPoints.clear();

		if(application.getIsWalking()){
			
			homeSafeButton.setText("Home Safe");
		}else{
			homeSafeButton.setText("Start Walk");
		}
		final List<LatLng> geoPoints = new ArrayList<LatLng>();
		emergencyUserNameList.clear();

		if (markersList == null) {
			markersList = new HashMap<String, Marker>();
		}

		for (final ParseUser user : emergencyContactsList) {
			// Log.d("demo",
			// "Emergency contact name" + user.getString("userRealName")
			// + user.getObjectId());
			emergencyUserNameList.add(user.getString("userRealName"));
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Walk");
			query.whereEqualTo("user", user);
			query.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					// each emergency contact should have just one walk entry
					ParseObject walkTrip = objects.get(0);

					ArrayList<ParseGeoPoint> points = new ArrayList<ParseGeoPoint>();
					points = (ArrayList<ParseGeoPoint>) walkTrip
							.get("walkPoints");
					String userSafetyStatus = walkTrip
							.getString("safetyStatus");

					for (ParseGeoPoint geoPoint : points) {
						LatLng obj = new LatLng(geoPoint.getLatitude(),
								geoPoint.getLongitude());
						geoPoints.add(obj);
					}
					emergencyContactsGeoPoints.put(
							user.getString("userRealName"), geoPoints);
					userSafetyStatusList.put(user.getString("userRealName"),
							userSafetyStatus);

					Log.d("demo", "User safety status : " + userSafetyStatus);
					if (userSafetyStatus == null) {
						userSafetyStatus = "dummy status";
					}
					if (isFirstTimeFetch) {
						// zoom and pan the map to include all the jog points
						gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
								getZooomedLatLngBoundsForGeoPoints(geoPoints)
										.build(), 50));
						isFirstTimeFetch = false;
					}

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							MapActivity.this,
							android.R.layout.simple_spinner_item,
							emergencyUserNameList);
					emergencyContactSpinner.setAdapter(adapter);
					emergencyContactSpinner
							.setOnItemSelectedListener(new OnItemSelectedListener() {

								@Override
								public void onItemSelected(
										AdapterView<?> parent, View view,
										int position, long id) {
									// //cleanup all markers on the map
									removeMarkerForAllUsers();
									currentSelectedUser = emergencyContactsList
											.get(position).getString(
													"userRealName");

									String safetyStatus = userSafetyStatusList
											.get(currentSelectedUser);
									List<LatLng> geoPoints = emergencyContactsGeoPoints
											.get(currentSelectedUser);
									if ((geoPoints != null)
											&& (geoPoints.size() > 0))
										drawMarkerForUser(
												currentSelectedUser,
												geoPoints.get(geoPoints.size() - 1),
												safetyStatus, true);
								}

								@Override
								public void onNothingSelected(
										AdapterView<?> parent) {
									// TODO Auto-generated method stub

								}
							});

					// set default selection for the spinner else the code below
					// will not work
					if (emergencyContactsList.size() > 0) {
						emergencyContactSpinner.setSelection(0);
					}

					if (currentSelectedUser != null) {
						String safetyStatus = userSafetyStatusList
								.get(currentSelectedUser);
						List<LatLng> currentUserGeoPoints = emergencyContactsGeoPoints
								.get(currentSelectedUser);
						drawMarkerForUser(currentSelectedUser,
								currentUserGeoPoints.get(currentUserGeoPoints
										.size() - 1), safetyStatus, true);
					}

				}
			});
		}

		// emergencyContactSpinner.setOnItemClickListener(new
		// OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// //cleanup all markers on the map
		// removeMarkerForAllUsers();
		//
		// String username =
		// emergencyContactsList.get(position).getString("userRealName");
		// String safetyStatus = userSafetyStatusList.get(username);
		// List<LatLng> geoPoints = emergencyContactsGeoPoints.get(username);
		//
		// drawMarkerForUser(username,
		// geoPoints.get(geoPoints.size() - 1),
		// safetyStatus, true);
		//
		// }
		// });

	}

	private void drawMarkerForUser(String userName, LatLng geoPoint,
			String userStatus, boolean zoomToLocation) {

		MarkerOptions markerOptions = new MarkerOptions()
				.title(userName)
				.snippet(userStatus)
				.position(geoPoint)
				.icon(BitmapDescriptorFactory
						.defaultMarker(getMarkerColorForStatus(userStatus)));

		// remove previous marker for the user if any
		if (markersList.get(userName) != null) {
			markersList.get(userName).remove();
		}

		Marker marker = gMap.addMarker(markerOptions);
		marker.showInfoWindow();
		markersList.put(userName, marker);

		if (zoomToLocation) {

			// Also including the location of mine in the map and drawing my
			// marker
			ArrayList<LatLng> latLngPoints = new ArrayList<LatLng>();

			if (application.getCurrentLocation() != null) {
				LatLng currentLocation = new LatLng(application
						.getCurrentLocation().getLatitude(), application
						.getCurrentLocation().getLongitude());

				MarkerOptions myMarkerOptions = new MarkerOptions()
						.position(currentLocation)
						.icon(BitmapDescriptorFactory
								.fromResource(getMyLocationMarkerForSafetyStatus(application.getCurrentSafetyStatus())));
				if (myMarker != null) {
					myMarker.remove();
				}// else{
				myMarker = gMap.addMarker(myMarkerOptions);
				// myMarker.showInfoWindow();
				// }
				latLngPoints.add(currentLocation);
			}
			latLngPoints.add(geoPoint);

			gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
					getZooomedLatLngBoundsForGeoPoints(latLngPoints).build(),
					300));
			// Zoom in the Google Map
			// gMap.animateCamera(CameraUpdateFactory.zoomTo(15));
		}

	}

	private void drawPolyLinesForUser(String userName, List<LatLng> geoPoints,
			String userSafetyStatus) {

		Log.d("demo", "Drawing lines for " + userName);
		int polyLineColor = Color.GRAY;
		if (userSafetyStatus.equals("Risk")) {
			polyLineColor = Color.RED;

		} else if (userSafetyStatus.equals("Little Risk")) {

			polyLineColor = Color.YELLOW;

		} else if (userSafetyStatus.equals("Safe")) {
			polyLineColor = Color.GREEN;
		}

		PolylineOptions rectOptions = new PolylineOptions()
				.color(polyLineColor);

		for (LatLng latLng : geoPoints) {
			rectOptions.add(latLng);
		}
		// polylines.add(gMap.addPolyline(rectOptions));
		gMap.addPolyline(rectOptions);

	}

	private LatLngBounds.Builder getZooomedLatLngBoundsForGeoPoints(
			List<LatLng> geoPoints) {
		LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();

		for (LatLng item : geoPoints) {
			latLngBounds.include(item);
		}
		return latLngBounds;
	}

	private void fetchWalkInfo() {
		SafeWalkApplication application = (SafeWalkApplication) getApplication();
		application.checkForWalkingUsers(this);
	}

	public void onLocationOffClicked(View v) {
//		if(locationBroadcastSwitch.isChecked()){
//			application.turnOnLocationUpdates();
//		}else{
//
//			application.turnOffLocationUpdates();
//		}

	}

	public void onEmergencyButtonClick(View v) {

		final Location location = application.getCurrentLocation();
		new AlertDialog.Builder(this)
				.setTitle("Requesting Emergency")
				.setMessage(
						"Are you sure you want to send Emergency requests to all the Emergency contacts? Remember your emergency situation will be notified to the Campus Police and the Charlotte Police Department")
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								final String messageToSend;
								if (location == null) {
									messageToSend = "Hey I am in an emergency situation ! Please Help Me. ";
								} else {
									messageToSend = "Hey I am in an emergency situation ! Please Help Me ! My coordinates are Longtitude:"
											+ location.getLongitude()
											+ " Latitude:"
											+ location.getLatitude();
								}

								// fetch the numbers
								for (ParseUser user : emergencyContactList) {
									ParseQuery<ParseObject> emergencyQuery = ParseQuery
											.getQuery("EmergencyContact");
									emergencyQuery.whereEqualTo("userID",
											user.getString("userID"));
									emergencyQuery
											.getFirstInBackground(new GetCallback<ParseObject>() {

												@Override
												public void done(
														ParseObject object,
														ParseException e) {
													Log.d("demo",
															object.getString("contact"));
													SmsManager
															.getDefault()
															.sendTextMessage(
																	object.getString("contact"),
																	null,
																	messageToSend,
																	null, null);

												}
											});

								}

							}
						})
				.setNegativeButton(android.R.string.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
							}
						}).setIcon(android.R.drawable.ic_dialog_alert).show();
		Toast.makeText(MapActivity.this, "Help Requested", Toast.LENGTH_SHORT)
				.show();

	}

	public void onHomeSafeButtonClick(View v) {
		safetyStatusSpinner.setSelection(getIndexForStatus("Safe"));
		ParseUser currentUser = ParseUser.getCurrentUser();
		currentUser.put("isWalking", false);
		currentUser.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				Log.d("demo", "updated walking status to false");

			}
		});

	}

	private int getIndexForStatus(String status) {
		if (status.equals("Safe")) {
			return 2;
		} else if (status.equals("Little Risk")) {
			return 1;
		} else {
			return 0;
		}
	}

	private String getStatusForIndex(int position) {

		switch (position) {
		case 2:
			return "Safe";
		case 1:
			return "Little Risk";
		case 0:
			return "Risk";
		default:
			return "Safe";

		}
	}

	private void removeMarkerForAllUsers() {

		Iterator it = markersList.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			System.out.println(pairs.getKey() + " = " + pairs.getValue());
			Marker marker = (Marker) (pairs.getValue());
			marker.remove();
			it.remove(); // avoids a ConcurrentModificationException
		}

	}

	// private void addMyMarker(){
	// if(application.getCurrentLocation() != null){
	// drawMarkerForUser("Me", geoPoint, application.getCurrentSafetyStatus(),
	// zoomToLocation);
	// }
	//
	// }
	//
	private float getMarkerColorForStatus(String status) {

		float markerColor = BitmapDescriptorFactory.HUE_AZURE;
		if (status.equals("Risk")) {
			markerColor = BitmapDescriptorFactory.HUE_RED;

		} else if (status.equals("Little Risk")) {

			markerColor = BitmapDescriptorFactory.HUE_YELLOW;

		} else if (status.equals("Safe")) {
			markerColor = BitmapDescriptorFactory.HUE_GREEN;
		}
		return markerColor;

	}
	private int getMyLocationMarkerForSafetyStatus(String status){

		if (status.equals("Risk")) {
			return R.drawable.my_location_risk;

		} else if (status.equals("Little Risk")) {

			return R.drawable.my_location_little_risk;

		} else if (status.equals("Safe")) {
			return R.drawable.my_location_safe;
		}

		return R.drawable.my_location_safe;
	}

}
