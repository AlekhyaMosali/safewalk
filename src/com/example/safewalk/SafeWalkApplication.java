/**
 * Copyright 2010-present Facebook.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.safewalk;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

// We use a custom Application class to store our minimal state data (which users have been selected).
// A real-world application will likely require a more robust data model.
public class SafeWalkApplication extends Application {
	
    public static final int EDIT_FRIENDS_ACTION = 1001;
    public static final int PICK_FRIENDS_DONE_ACTION = 1002;
    
	@Override
	public void onCreate() {
	    Parse.initialize(this, "6YFFWaGtC4EcyBW6RQOcGNOdRLoNDXQDZMfmQqWH", "qZgNu7OCSPVaITRMVveXxHxK8I1uNhq2iDIBVklL");
		ParseFacebookUtils.initialize(getString(R.string.facebook_app_id));
		
		
		ParsePush.subscribeInBackground("", new SaveCallback() {
			  @Override
			  public void done(ParseException e) {
			    if (e == null) {
			      Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
			    } else {
			      Log.e("com.parse.push", "failed to subscribe for push", e);
			    }
			  }
			});
		
		registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
			
			@Override
			public void onActivityStopped(Activity activity) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onActivityStarted(Activity activity) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onActivityResumed(Activity activity) {
				Log.d("demo", activity.getLocalClassName() + "started");
				//checkForLocationServicesEnabled();
				
			}
			
			@Override
			public void onActivityPaused(Activity activity) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onActivityDestroyed(Activity activity) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				
			}
		});
		super.onCreate();
	}
	
	public void checkForWalkingUsers(final IEmergencyContactsFetch activity){
		this.activity = activity;
	ParseQuery<ParseObject> query = ParseQuery.getQuery("EmergencyContact");
	query.whereEqualTo("user", currentUser);
	query.findInBackground(new FindCallback<ParseObject>() {
		
		@Override
		public void done(List<ParseObject> objects, ParseException e) {
			//list of all the emergency contacts of the user
			if(objects.size() >0){
				final ArrayList<ParseObject> emergencyContactList = (ArrayList<ParseObject>) objects;
				ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
				userQuery.findInBackground(new FindCallback<ParseUser>() {

					@Override
					public void done(List<ParseUser> objects,
							ParseException e) {
						
						if(objects.size()>0){
							emergencyParseUserContacts = new ArrayList<ParseUser>();
							for (ParseUser user :objects){
								for (ParseObject emergencyContact:emergencyContactList){
									String userId = user.getString("userID");
									String emergencyUserId = emergencyContact.getString("userID");
									if(userId == null ? emergencyUserId == null : userId.equals(emergencyUserId)){
										emergencyParseUserContacts.add(user);
									}
								}
							}
							if(emergencyParseUserContacts.size()>0){
								
								//set the list of emergency parse users
//								Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
//								startActivity(intent);
							}else{
//								Intent intent = new Intent(getApplicationContext(), ConfigureFriendsActivity.class);
//								startActivity(intent);
							}
							setIsWalking((Boolean)currentUser.get("isWalking")) ;
							activity.fetchDone(emergencyParseUserContacts);
						}
					}
					
				});
			}else{
//				Intent intent = new Intent(getApplicationContext(), ConfigureFriendsActivity.class);
//				startActivity(intent);
			}
		}
	});
	}

	ParseUser currentUser;
	ArrayList<ParseUser> emergencyParseUserContacts;
	String userName;
	
    private List<GraphUser> selectedUsers;
    
	LocationManager mLocationMngr;
	LocationListener mLocListener;
	ArrayList<ParseGeoPoint> walkPoints;
	ProgressDialog progressDialog;
	IEmergencyContactsFetch activity;
	LocationManager mLocationManager;
	LocationListener mLocationListener;
	Boolean isWalking;
	
	Location currentLocation;
	
	String currentSafetyStatus;
	
//	public SafeWalkApplication (IEmergencyContactsFetch activity) {
//		this.activity = activity;
//	}

    public String getCurrentSafetyStatus() {
		return currentSafetyStatus;
	}

	public void setCurrentSafetyStatus(String currentSafetyStatus) {
		this.currentSafetyStatus = currentSafetyStatus;
	}

	public List<GraphUser> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(List<GraphUser> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

	static public interface IEmergencyContactsFetch {
		void fetchDone(ArrayList<ParseUser> emergencyContactsList);
	}

	public Boolean getIsWalking() {
		return isWalking;
	}

	public void setIsWalking(Boolean isWalking) {
		this.isWalking = isWalking;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}
	public void turnOffLocationUpdates(){
		this.mLocationManager.removeUpdates(mLocListener);
	}
	
	public void turnOnLocationUpdates(){

		this.mLocationMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 100, mLocListener);
	}
	//start publishing location events

	public LocationManager getmLocationMngr() {
		return mLocationMngr;
	}

	public void setmLocationMngr(LocationManager mLocationMngr) {
		this.mLocationMngr = mLocationMngr;
	}

	public LocationListener getmLocListener() {
		return mLocListener;
	}

	public void setmLocListener(LocationListener mLocListener) {
		this.mLocListener = mLocListener;
	}
	
	
}
