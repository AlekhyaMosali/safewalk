/*This activity populates all the Emergency contacts that the user selected from 
 * the Facebook Friends picker. All the friends constitute the users emergency contacts.
 * 
 */
package com.example.safewalk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class SafetyNetwork extends Activity {
	ArrayList<String> usersNameList;
	ArrayList<String> userIdList;
	ListView friendsListView;

	 HashMap<String, Boolean> checkPhoneNumberMap = new HashMap<String, Boolean>();
	 HashMap<String, Boolean> emergencyContactList = new HashMap<String, Boolean>();
	boolean hasInstalled;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_safety_network);
		/*usersNameList = new ArrayList<String>();
		userIdList = new ArrayList<String>();

		if (getIntent().getExtras().getInt("intentAction") == SafeWalkApplication.EDIT_FRIENDS_ACTION) {
//			SafeWalkApplication application = (SafeWalkApplication) getApplication();
//			application.checkForRunningUsers(activity)
			
			ParseUser user = ParseUser.getCurrentUser();
			ParseQuery<ParseObject> emergencyQuery = ParseQuery
					.getQuery("EmergencyContact");
			emergencyQuery.whereEqualTo("user",
					user);
			emergencyQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					for (ParseObject obj: objects){
						if(obj.getString("contact").equals("")){
							checkPhoneNumberMap.put(obj.getString("name"), false);
						}else{
							Log.d("demo"," Number"+obj.getString("contact"));
							checkPhoneNumberMap.put(obj.getString("name"), true);
						}
						usersNameList.add(obj.getString("name"));
						userIdList.add(obj.getString("userID"));
					}
					
					friendsListView = (ListView) findViewById(R.id.ListView1);
					final ArrayAdapter<String> usersNameAdapter = new FriendsAdapter(usersNameList,checkPhoneNumberMap);
					friendsListView.setAdapter(usersNameAdapter);
					
					friendsListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {

							Intent intent = new Intent(SafetyNetwork.this,
									EmergencyContactDetails.class);
							intent.putExtra("name", usersNameList.get(position));
							intent.putExtra("userGraphID",
									userIdList.get(position));

							startActivity(intent);

						}
					});
			usersNameAdapter.setNotifyOnChange(true);
					
				}
			});
			
			

		} else if (getIntent().getExtras().getInt("intentAction") == SafeWalkApplication.PICK_FRIENDS_DONE_ACTION) {

			SafeWalkApplication application = (SafeWalkApplication) getApplication();

			final List<GraphUser> selectedUsers = application
					.getSelectedUsers();

			for (GraphUser user : selectedUsers) {
				usersNameList.add(user.getName());
				userIdList.add(user.getId());
				checkPhoneNumberMap.put(user.getName(), false);
				
			}
			
			friendsListView = (ListView) findViewById(R.id.ListView1);

			final ArrayAdapter<String> usersNameAdapter = new FriendsAdapter(usersNameList,checkPhoneNumberMap);

//			ParseQuery<ParseObject> checkPhoneNoQurey = ParseQuery
//					.getQuery("EmergencyContact");
//			checkPhoneNoQurey.whereEqualTo("user", ParseUser.getCurrentUser());
//			checkPhoneNoQurey.findInBackground(new FindCallback<ParseObject>() {
//				public void done(List<ParseObject> objects, ParseException e) {
//					for (ParseObject object : objects) {
//						try {
//							object.fetchIfNeeded();
//						} catch (ParseException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
//						for (GraphUser user : selectedUsers) {
//							String contact = object.getString("contact");
//							if (contact.equals("")) {
//								checkPhoneNumberMap.put(user.getName(), false);
//							} else {
//								checkPhoneNumberMap.put(user.getName(), true);
//							}
//						}

						friendsListView.setAdapter(usersNameAdapter);

//					}
//
//				}
//			});
			friendsListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {

							Intent intent = new Intent(SafetyNetwork.this,
									EmergencyContactDetails.class);
							intent.putExtra("name", usersNameList.get(position));
							intent.putExtra("userGraphID",
									userIdList.get(position));

							startActivity(intent);

						}
					});
			usersNameAdapter.setNotifyOnChange(true);
		}

		
		 * //to check if the selected emergency contact is using SafeWalk app
		 * //if not, can be used to alert the user that particular emergency
		 * contact is not using the app ParseQuery<ParseUser> userQuery =
		 * ParseUser.getQuery(); userQuery.findInBackground(new
		 * FindCallback<ParseUser>() {
		 * 
		 * @Override public void done(List<ParseUser> objects, ParseException e)
		 * {
		 * 
		 * for (ParseUser object : objects) { try { object.fetchIfNeeded(); }
		 * catch (ParseException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); } for (GraphUser user : selectedUsers) { String
		 * grahUserId = object.getString("userID"); if (grahUserId != null &&
		 * grahUserId.equals(user.getId())) {
		 * emergencyContactList.put(user.getName(), true); } else if (grahUserId
		 * == null) { emergencyContactList.put(user.getName(), false); } }
		 * 
		 * friendsListView.setAdapter(usersNameAdapter);
		 * 
		 * } } });
		 
*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.safety_network, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	class FriendsAdapter extends ArrayAdapter<String> {
		
		ArrayList<String> nameList;
		HashMap<String, Boolean> isValidContactList;

		public FriendsAdapter(ArrayList<String> nameList, HashMap<String, Boolean> isValidContactList) {
			super(SafetyNetwork.this, R.layout.row_item_layout,
					R.id.textViewName, nameList);
			
			this.nameList = nameList;
			this.isValidContactList = isValidContactList;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = super.getView(position, convertView, parent);

			ImageView icon = (ImageView) row.findViewById(R.id.imageViewStatus);
			if(isValidContactList.get(nameList.get(position))){
				icon.setImageResource(android.R.drawable.ic_menu_info_details);				
			}else{
				icon.setImageResource(android.R.drawable.ic_dialog_alert);				
			}

//			if (checkPhoneNumberMap.get(usersNameList.get(position)) == true) {
//				icon.setImageResource(android.R.drawable.ic_menu_info_details);
//			} else if (checkPhoneNumberMap.get(usersNameList.get(position)) == false) {
//				icon.setImageResource(android.R.drawable.ic_dialog_alert);
//			}
			return row;
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		setContentView(R.layout.activity_safety_network);
		usersNameList = new ArrayList<String>();
		userIdList = new ArrayList<String>();

		if (getIntent().getExtras().getInt("intentAction") == SafeWalkApplication.EDIT_FRIENDS_ACTION) {
//			SafeWalkApplication application = (SafeWalkApplication) getApplication();
//			application.checkForRunningUsers(activity)
			
			ParseUser user = ParseUser.getCurrentUser();
			ParseQuery<ParseObject> emergencyQuery = ParseQuery
					.getQuery("EmergencyContact");
			emergencyQuery.whereEqualTo("user",
					user);
			emergencyQuery.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					for (ParseObject obj: objects){
						if(obj.getString("contact").equals("")){
							checkPhoneNumberMap.put(obj.getString("name"), false);
						}else{
							Log.d("demo"," Number"+obj.getString("contact"));
							checkPhoneNumberMap.put(obj.getString("name"), true);
						}
						usersNameList.add(obj.getString("name"));
						userIdList.add(obj.getString("userID"));
					}
					
					friendsListView = (ListView) findViewById(R.id.ListView1);
					final ArrayAdapter<String> usersNameAdapter = new FriendsAdapter(usersNameList,checkPhoneNumberMap);
					friendsListView.setAdapter(usersNameAdapter);
					
					friendsListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {

							Intent intent = new Intent(SafetyNetwork.this,
									EmergencyContactDetails.class);
							intent.putExtra("name", usersNameList.get(position));
							intent.putExtra("userGraphID",
									userIdList.get(position));

							startActivity(intent);

						}
					});
			usersNameAdapter.setNotifyOnChange(true);
					
				}
			});
			
			

		} else if (getIntent().getExtras().getInt("intentAction") == SafeWalkApplication.PICK_FRIENDS_DONE_ACTION) {

			SafeWalkApplication application = (SafeWalkApplication) getApplication();

			final List<GraphUser> selectedUsers = application
					.getSelectedUsers();

			for (GraphUser user : selectedUsers) {
				usersNameList.add(user.getName());
				userIdList.add(user.getId());
				checkPhoneNumberMap.put(user.getName(), false);
				
			}
			
			friendsListView = (ListView) findViewById(R.id.ListView1);

			final ArrayAdapter<String> usersNameAdapter = new FriendsAdapter(usersNameList,checkPhoneNumberMap);

//			ParseQuery<ParseObject> checkPhoneNoQurey = ParseQuery
//					.getQuery("EmergencyContact");
//			checkPhoneNoQurey.whereEqualTo("user", ParseUser.getCurrentUser());
//			checkPhoneNoQurey.findInBackground(new FindCallback<ParseObject>() {
//				public void done(List<ParseObject> objects, ParseException e) {
//					for (ParseObject object : objects) {
//						try {
//							object.fetchIfNeeded();
//						} catch (ParseException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
//						for (GraphUser user : selectedUsers) {
//							String contact = object.getString("contact");
//							if (contact.equals("")) {
//								checkPhoneNumberMap.put(user.getName(), false);
//							} else {
//								checkPhoneNumberMap.put(user.getName(), true);
//							}
//						}

						friendsListView.setAdapter(usersNameAdapter);

//					}
//
//				}
//			});
			friendsListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {

							Intent intent = new Intent(SafetyNetwork.this,
									EmergencyContactDetails.class);
							intent.putExtra("name", usersNameList.get(position));
							intent.putExtra("userGraphID",
									userIdList.get(position));

							startActivity(intent);

						}
					});
			usersNameAdapter.setNotifyOnChange(true);
		}

		/*
		 * //to check if the selected emergency contact is using SafeWalk app
		 * //if not, can be used to alert the user that particular emergency
		 * contact is not using the app ParseQuery<ParseUser> userQuery =
		 * ParseUser.getQuery(); userQuery.findInBackground(new
		 * FindCallback<ParseUser>() {
		 * 
		 * @Override public void done(List<ParseUser> objects, ParseException e)
		 * {
		 * 
		 * for (ParseUser object : objects) { try { object.fetchIfNeeded(); }
		 * catch (ParseException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); } for (GraphUser user : selectedUsers) { String
		 * grahUserId = object.getString("userID"); if (grahUserId != null &&
		 * grahUserId.equals(user.getId())) {
		 * emergencyContactList.put(user.getName(), true); } else if (grahUserId
		 * == null) { emergencyContactList.put(user.getName(), false); } }
		 * 
		 * friendsListView.setAdapter(usersNameAdapter);
		 * 
		 * } } });
		 */
	}

}
