package com.example.safewalk;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class TakeAWalkActivity extends Activity {

	String currentSafetyStatus;
	String[] safetyStatusList = new String[]{"Risk", "Little Risk", "Safe"};
	LocationManager mLocationMngr;
	LocationListener mLocListener;
	SafeWalkApplication application;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_awalk);
		application = (SafeWalkApplication) getApplication();


//		application.checkForLocationServicesEnabled();

		
		Spinner dropdown = (Spinner)findViewById(R.id.spinner1);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_list_view, safetyStatusList);
		dropdown.setAdapter(adapter);
		
		dropdown.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub 
				currentSafetyStatus = safetyStatusList[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				currentSafetyStatus = safetyStatusList[0];
			}
		});
	}
	
	
	@Override
	protected void onResume() {
		mLocationMngr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		checkForLocationServicesEnabled();
		super.onResume();
	}


	public void onStartWalkingButtonClicked(View v){
		
		final SafeWalkApplication app = (SafeWalkApplication) getApplication();
		app.setCurrentSafetyStatus(currentSafetyStatus);
		
		final ParseUser currentUser = ParseUser.getCurrentUser();
		//notify all the emergency contacts
		ArrayList<ParseUser> emergencyContacts =  application.emergencyParseUserContacts;
		ParsePush parsePush = new ParsePush();
		ParseQuery pQuery = ParseInstallation.getQuery(); // <-- Installation query
		pQuery.whereContainedIn("user", emergencyContacts); // <-- you'll probably want to target someone that's not the current user, so modify accordingly
		parsePush.sendMessageInBackground(application.userName+" has started walking and safety status is "+currentSafetyStatus, pQuery);
		
		//start publishing location events
		mLocationMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 100, mLocListener);

		
		ParseQuery<ParseObject> walkQuery = ParseQuery.getQuery("Walk");
		walkQuery.whereEqualTo("user", currentUser);
		walkQuery.getFirstInBackground(new GetCallback<ParseObject>() {
			
			@Override
			public void done(ParseObject object, ParseException e) {
				if(object == null){
					//the user has not Walk table entry- thus user is walking for the first time
					 object = new ParseObject("Walk");

					 object.put("user", currentUser);
					 object.put("safetyStatus", app.getCurrentSafetyStatus());


					
					Log.d("demo", "Safety status :"+currentSafetyStatus+" updated for the user"+ currentUser.getUsername());
				}else{
					object.put("safetyStatus", app.getCurrentSafetyStatus());

				}
				

				currentUser.put("isWalking", true);
				 application.setIsWalking(true);
				 currentUser.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {
						Log.d("demo", "isWalking saved");
						
					}
				});
				 
				object.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {
						
						//TODO: good place to put a progress dialog
						
						Intent intent = new Intent(TakeAWalkActivity.this, MapActivity.class);
						intent.putExtra("walkStatus", app.getCurrentSafetyStatus());
						intent.putExtra("locationBroadcast", true);
						startActivity(intent);
						
					}
				});
				
				
			}
		});
		

		
	}
	
	private void checkForLocationServicesEnabled() {

		if(!mLocationMngr.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("GPS Not Enabled")
			.setMessage("Would you like to enable GPS from settings")
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivity(intent);
					
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
					//finish();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
				

		}else{
			mLocListener = new LocationListener() {
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLocationChanged(final Location location) {
					
					final SafeWalkApplication application = (SafeWalkApplication) getApplication();
					
					application.setCurrentLocation(location);

					Log.d("demo", location.getLatitude() + "," +location.getLongitude() );
					
					final ParseGeoPoint newWalkPoint = new ParseGeoPoint(location.getLatitude(),location.getLongitude());
					ParseQuery<ParseObject> walkQuery = ParseQuery.getQuery("Walk");
					walkQuery.whereEqualTo("user", ParseUser.getCurrentUser());
					walkQuery.findInBackground(new FindCallback<ParseObject>() {
						
						@Override
						public void done(List<ParseObject> objects, ParseException e) {
							// TODO Auto-generated method stub
							

							if (objects.size() > 0){
								ArrayList<ParseGeoPoint> walkPoints =  (ArrayList<ParseGeoPoint>) objects.get(0).get("walkPoints");
								walkPoints.add(newWalkPoint);
								objects.get(0).put("safetyStatus", application.getCurrentSafetyStatus());
								objects.get(0).put("walkPoint", newWalkPoint);
								Log.d("demo","location changed: "+ newWalkPoint.toString());
								objects.get(0).saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										// TODO Auto-generated method stub
										Log.d("demo", "Location sccessfully saved in Parse.");
									}
								});

								
							}else{
								//the users first walk
								
								ParseObject walk = new ParseObject("Walk");

								walk.add("walkPoints", newWalkPoint);
								walk.put("walkPoint", newWalkPoint);
								walk.put("user", ParseUser.getCurrentUser());
								walk.put("safetyStatus",  application.getCurrentSafetyStatus());
								
								walk.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										

										// TODO Auto-generated method stub
										Log.d("demo", "Location sccessfully saved in Parse.");
									}
								});
								
							}

							application.walkPoints.add(newWalkPoint);

						}
					});

//					walk.put("user", application.);
					
					
				}
			};

//			application.mLocationMngr = mLocationMngr;
//			application.mLocListener = mLocListener;
			application.setmLocationMngr(mLocationMngr);
			application.setmLocListener(mLocListener);
		}

	}
	
	
	
	
}
