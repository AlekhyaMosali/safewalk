# [Safewalk](https://www.youtube.com/watch?v=fq-ab1xjb_A) #

App Preview Link : [https://youtu.be/fq-ab1xjb_A](https://youtu.be/fq-ab1xjb_A)

## What is SafeWalk ? ##
SafeWalk enable users to request for help from their contacts in an emergency situation. It helps contacts of a user to monitor the users position and ensure that they are safe especially while walking through risky streets. The main functionality of the application is to enable users to request for help from their contacts in an emergency situation. It helps contacts of a user to monitor the users position and ensure that they are safe especially while walking through risky streets. In case of an emergency, the contacts of the user can reach or send for help to the user’s place of emergency.

## Why do we need SafeWalk? ##
Often at times of emergencies, we hardly get time to make a call or message our contacts about the emergency. This application will constantly send location information to the emergency contacts so that they can monitor the location of the user. In an emergency situation such as a mugging attack with  the single click of a button, this application emergency alerts the contacts of the user. In the case when a mugger robs a phone and that the application is still running, it could be used to track the muggers position and catch hold of the mugger. So this application is a must for safety of the user and assurance to the near and dear ones about the users safety.

## Who needs SafeWalk? ##
Any user who feels risky while walking especially students who walk home late after class, evening walk commuters, joggers. It can also be used by near and dear ones of the user to keep track of the safety of the user.

## How SafeWalk operates? ##
* The application will use Facebook API to authenticate the users and connect to the emergency contacts.
* The application will use Parse.com to store user content.
* The application will use Google’s Map Services to show the MapView

## Screenshots with App description ##

### Onboarding ###
![Screen Shot 2015-11-08 at 8.12.55 PM.png](https://bitbucket.org/repo/7Gk9nz/images/1787242682-Screen%20Shot%202015-11-08%20at%208.12.55%20PM.png)


Since SafeWalk is an Emergency application, it requires that users configure at least one emergency contact to start using the features of the application. So during the initial on boarding, users are asked to configure their emergency contacts. If the user has chosen a friend who is using the SafeWalk Application, it will direct to the Main Menu and the user can start using the application. The application authenticates users using their Facebook accounts. So the users of the application are assumed to have
Facebook accounts.

### Main Menu ###
![Screen Shot 2015-11-08 at 8.16.05 PM.pn![Screen Shot 2015-11-08 at 8.21.32 PM.png](https://bitbucket.org/repo/7Gk9nz/images/2155681257-Screen%20Shot%202015-11-08%20at%208.21.32%20PM.png)g](https://bitbucket.org/repo/7Gk9nz/images/1302283605-Screen%20Shot%202015-11-08%20at%208.16.05%20PM.png)

### Taking a Walk ###
![Screen Shot 2015-11-08 at 8.19.33 PM.png](https://bitbucket.org/repo/7Gk9nz/images/718998139-Screen%20Shot%202015-11-08%20at%208.19.33%20PM.png)

The user has three choices to determine his/her safety status - Risk, Little Risk and Safe. When user chooses to “Start Walking”, a push notification is sent to all the members in the safety network of the user and afterwards, the location of the user is broadcasted to the safety network. The emergency contact receives the push notification.

![Screen Shot 2015-11-08 at 8.21.32 PM.png](https://bitbucket.org/repo/7Gk9nz/images/2658175498-Screen%20Shot%202015-11-08%20at%208.21.32%20PM.png)

On clicking the notification will enable them to see the position of the user who had sent the Emergency status. The following colours have been used for the markers to signify the users emergency status.

![Screen Shot 2015-11-08 at 8.23.25 PM.png]
(https://bitbucket.org/repo/7Gk9nz/images/131926327-Screen%20Shot%202015-11-08%20at%208.23.25%20PM.png)

![Screen Shot 2015-11-08 at 8.24.15 PM.png]
(https://bitbucket.org/repo/7Gk9nz/images/1776980141-Screen%20Shot%202015-11-08%20at%208.24.15%20PM.png)

An Emergency contact when “Taking a Walk” can also see the his/her own position on the map. He/She has the ability to see the emergency contacts position and status on the map. The different emergency contacts can be chosen from the Emergency Contact spinner. Any user can change his/her emergency status using the spinner buttons. This will send a notification to all the members in the safety network of the user.

![Screen Shot 2015-11-08 at 8.25.10 PM.png](https://bitbucket.org/repo/7Gk9nz/images/954349123-Screen%20Shot%202015-11-08%20at%208.25.10%20PM.png)

Clicking on the Emergency button at the bottom will send an SMS requesting help to all the emergency contacts with the current GPS coordinates of the user. Also help request will be sent to the local police.